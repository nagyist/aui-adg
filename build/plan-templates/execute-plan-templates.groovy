plan(key:'PLAN',name:'Execute plan templates') {
    project(key:'AUI',name:'AUI')

    repository(name:'AUI-ADG')

    trigger(type:'polling',strategy:'periodically',frequency:'180') {
        repository(name:'AUI-ADG')

    }
    stage(name:'Default Stage') {
        job(key:'JOB1',name:'Default Job') {
            task(type:'checkout',description:'Checkout AUI-ADG') {
                repository(name:'AUI-ADG')

            }
            task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugin.bamboo-plan-templates:com.atlassian.bamboo.plugin.plantemplates',
                description:'Execute plan templates',template:'build/plan-templates/execute-plan-templates.groovy',
                passwordVariableCheck:'true',passwordVariable:'${bamboo.plan.template.rollout.bot.password}',
                executionStrategy:'executionStrategy.executeOnMaster',
                bambooServer:'https://ecosystem-bamboo.internal.atlassian.com',
                username:'${bamboo.plan.template.rollout.bot.username}')

            task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugin.bamboo-plan-templates:com.atlassian.bamboo.plugin.plantemplates',
                description:'Unit tests (Chrome)',template:'build/plan-templates/unit-tests-chrome.groovy',
                passwordVariableCheck:'true',passwordVariable:'${bamboo.plan.template.rollout.bot.password}',
                executionStrategy:'executionStrategy.executeOnMaster',
                bambooServer:'https://ecosystem-bamboo.internal.atlassian.com',
                username:'${bamboo.plan.template.rollout.bot.username}')

            task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugin.bamboo-plan-templates:com.atlassian.bamboo.plugin.plantemplates',
                description:'Unit tests (Firefox)',template:'build/plan-templates/unit-tests-firefox.groovy',
                passwordVariableCheck:'true',passwordVariable:'${bamboo.plan.template.rollout.bot.password}',
                executionStrategy:'executionStrategy.executeOnMaster',
                bambooServer:'https://ecosystem-bamboo.internal.atlassian.com',
                username:'${bamboo.plan.template.rollout.bot.username}')

            task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugin.bamboo-plan-templates:com.atlassian.bamboo.plugin.plantemplates',
                description:'Documentation staging',template:'build/plan-templates/documentation-staging.groovy',
                passwordVariableCheck:'true',executionStrategy:'executionStrategy.executeOnMaster',
                passwordVariable:'${bamboo.plan.template.rollout.bot.password}',
                bambooServer:'https://ecosystem-bamboo.internal.atlassian.com',
                username:'${bamboo.plan.template.rollout.bot.username}')

            task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugin.bamboo-plan-templates:com.atlassian.bamboo.plugin.plantemplates',
                description:'AUI / JIRA visual regression',
                template:'build/plan-templates/aui-jira-vis-reg.groovy',
                passwordVariableCheck:'true',passwordVariable:'${bamboo.plan.template.rollout.bot.password}',
                executionStrategy:'executionStrategy.executeOnMaster',
                bambooServer:'https://ecosystem-bamboo.internal.atlassian.com',
                username:'${bamboo.plan.template.rollout.bot.username}')

            task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugin.bamboo-plan-templates:com.atlassian.bamboo.plugin.plantemplates',
                description:'AUI / JIRA soke',template:'build/plan-templates/aui-jira-soke.groovy',
                passwordVariableCheck:'true',passwordVariable:'${bamboo.plan.template.rollout.bot.password}',
                executionStrategy:'executionStrategy.executeOnMaster',
                bambooServer:'https://ecosystem-bamboo.internal.atlassian.com',
                username:'${bamboo.plan.template.rollout.bot.username}')

            task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugin.bamboo-plan-templates:com.atlassian.bamboo.plugin.plantemplates',
                description:'Release',template:'build/plan-templates/release.groovy',
                passwordVariableCheck:'true',executionStrategy:'executionStrategy.executeOnMaster',
                passwordVariable:'${bamboo.plan.template.rollout.bot.password}',
                bambooServer:'https://ecosystem-bamboo.internal.atlassian.com',
                username:'${bamboo.plan.template.rollout.bot.username}')

        }
    }
    branchMonitoring() {
        createBranch()

        inactiveBranchCleanup(periodInDays:'30')

        deletedBranchCleanup(periodInDays:'30')

    }
    dependencies(triggerForBranches:'true')

    permissions() {
        anonymous(permissions:'read')

        loggedInUser(permissions:'read')

    }
}

