plan(key:'REL',name:'Release',description:'Releases AUI, deploys maven artefact and uploads to CDN, and builds the docs. There is a bamboo deployment associated that deploys the docs.') {
    project(key:'AUI',name:'AUI')

    repository(name:'AUI-ADG')

    repository(name:'AUI')

    label(name:'plan-templates')

    variable(key:'aui.dist.directory',value:'aui')

    variable(key:'aui.plan.branch',value:'master')

    variable(key:'next.release.version',value:'master')

    variable(key:'release.version',value:'null')

    notification(type:'All Builds Completed',recipient:'hipchat',
        apiKey:'${design.platform.hipchat.key.password}',
        notify:'false',room:'UI')

    notification(type:'All Builds Completed',recipient:'hipchat',
        apiKey:'${design.platform.hipchat.key.password}',
        notify:'false',room:'Design Platform')

    stage(name:'Release AUI') {
        job(key:'JOB1',name:'Release AUI',description:'Release the Open Source AUI') {
            requirement(key:'system.jdk.JDK 1.7',condition:'exists')

            requirement(key:'os',condition:'equals',value:'Linux')

            task(type:'checkout',description:'Checkout aui') {
                repository(name:'AUI',checkoutDirectory:'aui')

            }
            task(type:'npm',description:'npm install in aui',
                command:'install --no-color',executable:'Node.js',
                workingSubDirectory:'aui',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'Release AUI',scriptBody:'''
set -x
set -e

aui/build/bin/release.sh ${bamboo.aui.plan.branch} ${bamboo.release.version} ${bamboo.next.release.version}
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin" NPMPASSWORD=${bamboo.aui.public.npm.user.password}')

        }
    }
    stage(name:'Release AUI ADG') {
        job(key:'RA',name:'Release AUI-ADG') {
            requirement(key:'system.jdk.JDK 1.7',condition:'exists')

            requirement(key:'elastic',condition:'equals',value:'false')

            requirement(key:'os',condition:'equals',value:'Linux')

            artifactDefinition(name:'version',location:'aui-adg/',pattern:'version',
                shared:'true')

            artifactDefinition(name:'AUI ADG Dist',location:'aui-adg/dist/',
                pattern:'**/**',shared:'true')

            task(type:'script',description:'Install mvnvm',scriptBody:'''
set -x
set -e

mkdir -p ${bamboo.build.working.directory}/bin
curl -s https://bitbucket.org/mjensen/mvnvm/raw/mvnvm-1.0.3/mvn > ${bamboo.build.working.directory}/bin/mvn
chmod 0755 ${bamboo.build.working.directory}/bin/mvn
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'checkout',description:'Checkout aui',
                cleanCheckout:'true') {
                repository(name:'AUI',checkoutDirectory:'aui')

            }
            task(type:'script',description:'Checkout release version tag of aui',
                scriptBody:'''
set -x
set -e

git fetch git@bitbucket.org:atlassian/aui.git --tags
git checkout ${bamboo.release.version}
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"',
                workingSubDirectory:'aui')

            task(type:'checkout',description:'Checkout aui-adg') {
                repository(name:'AUI-ADG',checkoutDirectory:'aui-adg')

            }
            task(type:'npm',description:'npm install in aui',
                command:'install --no-color',executable:'Node.js',
                workingSubDirectory:'aui',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install bower in aui-adg',
                command:'install bower --no-color',executable:'Node.js',
                workingSubDirectory:'aui-adg',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'bower link aui in aui-adg',
                scriptBody:'''
set -x
set -e

cd aui
./node_modules/.bin/bower link --no-color
cd ../aui-adg
./node_modules/.bin/bower link aui --no-color
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install in aui-adg',
                command:'install --ignore-scripts --no-color',
                executable:'Node.js',workingSubDirectory:'aui-adg',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'bower install in aui-adg',
                scriptBody:'''
set -x
set -e

./node_modules/.bin/bower install --no-color
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"',
                workingSubDirectory:'aui-adg')

            task(type:'script',description:'Release Prepare AUI-ADG',
                scriptBody:'''
set -x
set -e

aui-adg/build/bin/release-prepare.sh ${bamboo.aui.plan.branch} ${bamboo.release.version}
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'Release AUI-ADG',
                scriptBody:'''
set -x
set -e

aui-adg/build/bin/release.sh ${bamboo.aui.plan.branch} ${bamboo.release.version} ${bamboo.next.release.version}
echo ${bamboo.release.version} > aui-adg/version
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

        }
    }
    stage(name:'Push AUI ADG to CDN') {
        job(key:'PAC',name:'Push to CDN') {
            requirement(key:'system.jdk.JDK 1.7',condition:'exists')

            requirement(key:'elastic',condition:'equals',value:'false')

            requirement(key:'system.builder.mvn3.Maven 3.0',condition:'exists')

            requirement(key:'os',condition:'equals',value:'Linux')

            artifactSubscription(name:'AUI ADG Dist',destination:'aui-adg-dist')

            task(type:'checkout',description:'Checkout aui',
                cleanCheckout:'true') {
                repository(name:'AUI',checkoutDirectory:'aui')

            }
            task(type:'checkout',description:'Checkout aui-adg') {
                repository(name:'AUI-ADG',checkoutDirectory:'aui-adg')

            }
            task(type:'script',description:'Checkout release version tag of aui and aui-adg',
                scriptBody:'''
set -x
set -e

cd aui
git fetch git@bitbucket.org:atlassian/aui.git --tags
git checkout ${bamboo.release.version}

cd ../aui-adg
git fetch git@bitbucket.org:atlassian/aui-adg.git --tags
git checkout ${bamboo.release.version}
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'Copy prebuilt aui-adg/dist to the correct directory',
                scriptBody:'''
set -x
set -e

cp -r aui-adg-dist aui-adg/dist
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install bower in aui',
                command:'install bower --no-color',executable:'Node.js',
                workingSubDirectory:'aui',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install bower in aui-adg',
                command:'install bower --no-color',executable:'Node.js',
                workingSubDirectory:'aui-adg',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'bower link aui in aui-adg',
                scriptBody:'''
set -x
set -e

cd aui
./node_modules/.bin/bower link --no-color
cd ../aui-adg
./node_modules/.bin/bower link aui --no-color
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install in aui',
                command:'install --no-color',executable:'Node.js',
                workingSubDirectory:'aui',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install in aui-adg',
                command:'install --ignore-scripts --no-color',
                executable:'Node.js',workingSubDirectory:'aui-adg',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'push-to-cdn',scriptBody:'''
set -x
set -e

./node_modules/.bin/gulp push-to-cdn --no-color --releaseVersion ${bamboo.release.version}
''',
                environmentVariables:'AWS_ACCESS_KEY_ID=${bamboo.aui.s3.key} AWS_SECRET_ACCESS_KEY=${bamboo.aui.s3.secret.password} JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"',
                workingSubDirectory:'aui-adg')

        }
    }
    stage(name:'Build Docs',description:'Builds the AUI docs') {
        job(key:'BAAD',name:'Build AUI ADG Docs') {
            requirement(key:'os',condition:'equals',value:'Linux')

            artifactDefinition(name:'AUI ADG Docs',location:'aui-adg/.tmp/docs/dist',
                pattern:'**/**',shared:'true')

            artifactSubscription(name:'AUI ADG Dist',destination:'aui-adg-dist')

            task(type:'script',description:'Install mvnvm',scriptBody:'''
set -x
set -e

mkdir -p ${bamboo.build.working.directory}/bin
curl -s https://bitbucket.org/mjensen/mvnvm/raw/mvnvm-1.0.3/mvn > ${bamboo.build.working.directory}/bin/mvn
chmod 0755 ${bamboo.build.working.directory}/bin/mvn
''',
                argument:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'checkout',description:'Checkout aui',
                cleanCheckout:'true') {
                repository(name:'AUI',checkoutDirectory:'aui')

            }
            task(type:'checkout',description:'Checkout aui-adg') {
                repository(name:'AUI-ADG',checkoutDirectory:'aui-adg')

            }
            task(type:'script',description:'Checkout release version tag of aui and aui-adg',
                scriptBody:'''
set -x
set -e

cd aui
git fetch git@bitbucket.org:atlassian/aui.git --tags
git checkout ${bamboo.release.version}

cd ../aui-adg
git fetch git@bitbucket.org:atlassian/aui-adg.git --tags
git checkout ${bamboo.release.version}
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'Copy prebuilt aui-adg/dist to the correct directory',
                scriptBody:'''
set -x
set -e

cp -r aui-adg-dist aui-adg/dist
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install bower in aui',
                command:'install bower --no-color',executable:'Node.js',
                workingSubDirectory:'aui',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install bower in aui-adg',
                command:'install bower --no-color',executable:'Node.js',
                workingSubDirectory:'aui-adg',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'bower link aui in aui-adg',
                scriptBody:'''
set -x
set -e

cd aui
./node_modules/.bin/bower link --no-color
cd ../aui-adg
./node_modules/.bin/bower link aui --no-color
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install in aui',
                command:'install --no-color',executable:'Node.js',
                workingSubDirectory:'aui',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm',description:'npm install in aui-adg',
                command:'install --ignore-scripts --no-color',
                executable:'Node.js',workingSubDirectory:'aui-adg',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'script',description:'Build Docs',scriptBody:'''
set -x
set -e

npm run docs/build --no-color
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"',
                workingSubDirectory:'aui-adg')

        }
    }
    dependencies(blockingStrategy:'notBlock',triggerForBranches:'true')

    permissions() {
        user(name:'buildeng-build-doctor',permissions:'read,write,build,clone,administration')

        anonymous(permissions:'read')

        loggedInUser(permissions:'read')

    }
}

deployment(name:'Deploy documentation (for release)',
    planKey:'AUI-REL') {
    versioning(version:'release-1',autoIncrementNumber:'true')

    environment(name:'Staging') {
        trigger(type:'afterSuccessfulPlan',description:'master')

        trigger(type:'afterSuccessfulPlan',description:'Stable')

        task(type:'addRequirement') {
            requirement(key:'os',condition:'equals',value:'Linux')

        }
        task(type:'cleanWorkingDirectory')

        task(type:'artifactDownload',description:'Download artifacts',
            planKey:'AUI-REL') {
            artifact(name:'AUI ADG Docs',localPath:'docs')

        }
        task(type:'artifactDownload',planKey:'AUI-REL') {
            artifact(name:'version',localPath:'.')

        }
        task(type:'script',description:'Upload Docs to staging - latest and version',
            scriptBody:'''
set -x
set -e

SERVER=docs-app.stg.internal.atlassian.com
URL=/var/www/domains/docs.atlassian.com/aui
VERSION=`cat version`
rsync -e \'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no\' -rl --delete docs/ uploads@$SERVER:$URL/$VERSION/
rsync -e \'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no\' -rl --delete docs/ uploads@$SERVER:$URL/latest/
''')

    }
    environment(name:'Production') {
        task(type:'addRequirement',description:'OS') {
            requirement(key:'os',condition:'equals',value:'Linux')

        }
        task(type:'cleanWorkingDirectory')

        task(type:'artifactDownload',description:'Download release contents',
            planKey:'AUI-REL') {
            artifact(name:'version',localPath:'.')

        }
        task(type:'artifactDownload',planKey:'AUI-REL') {
            artifact(name:'AUI ADG Docs',localPath:'docs')

        }
        task(type:'script',scriptBody:'''
set -x
set -e

SERVER=docs-app.internal.atlassian.com
URL=/var/www/domains/docs.atlassian.com/aui
VERSION=`cat version`
rsync -e \'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no\' -rl --delete docs/ uploads@$SERVER:$URL/$VERSION/
''')

    }
    environment(name:'Production - latest') {
        task(type:'addRequirement') {
            requirement(key:'os',condition:'equals',value:'Linux')

        }
        task(type:'cleanWorkingDirectory')

        task(type:'artifactDownload',description:'Download release contents',
            planKey:'AUI-REL') {
            artifact(name:'version',localPath:'.')

        }
        task(type:'artifactDownload',planKey:'AUI-REL') {
            artifact(name:'AUI ADG Docs',localPath:'docs')

        }
        task(type:'script',scriptBody:'''
set -x
set -e

SERVER=docs-app.internal.atlassian.com
URL=/var/www/domains/docs.atlassian.com/aui
rsync -e \'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no\' -rl --delete docs/ uploads@$SERVER:$URL/latest/
''')

    }
}
