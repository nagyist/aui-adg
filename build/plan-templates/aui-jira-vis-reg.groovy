plan(key:'AJVR', name:'AUI-JIRA Visual Regression') {
    project(key:'AUI',  name:'AUI')

    repository(name:'AUI-ADG')

    repository(name:'AUI')

    repository(name:'JIRA ci master stash')

    repository(name:'JIRA baseline images stash')

    label(name:'plan-templates')

    variable(key:'aui.jira.visreg.branch.name',  value:'AUI/JIRA-visual-regression')

    variable(key:'aui.plan.branch',  value:'master')

    variable(key:'destruction.plugin.version', value:'"not-relevant-for-master-branch"')

    variable(key:'produce.artifacts', value:'package -Dmaven.test.skip=true -pl jira-components/jira-webapp, jira-webdriver-tests -am -Pdistribution, jar-with-dependencies, maven3-statistics -Dfunc.mode.plugins -Dreference.plugins -DskipSources')

    trigger(type:'cron',description:'Nightly',cronExpression:'0 0 0 ? * *')

    notification(type:'Change of Build Status', recipient:'hipchat',
        apiKey:'${design.platform.hipchat.key.password}', notify:'false',
        room:'Design Platform')

    stage(name:'Build AUI') {
        job(key:'JOB1', name:'AUI-JIRA visual regression') {
            requirement(key:'system.jdk.JDK 1.7', condition:'exists')

            requirement(key:'system.builder.mvn3.Maven 3.0', condition:'exists')

            requirement(key:'system.builder.node.Node.js 0.12', condition:'exists')

            requirement(key:'system.builder.mvn3.Maven 3.2', condition:'exists')

            requirement(key:'os', condition:'equals', value:'Linux')

            artifactDefinition(name:'WebdriverTestHarness', location:'jira-distribution/jira-webdriver-tests-runner/target/surefire-reports',
                pattern:'*', shared:'false')

            artifactDefinition(name:'Reports', location:'jira-distribution/jira-webdriver-tests-runner/target/test-reports',
                pattern:'**', shared:'false')

            task(type:'checkout', description:'Check out AUI ADG') {
                repository(name:'AUI-ADG', checkoutDirectory:'aui-adg')
            }

            task(type:'checkout', description:'Check out AUI') {
                repository(name:'AUI', checkoutDirectory:'aui')
            }

            task(type:'checkout', description:'Check out JIRA') {
                repository(name:'JIRA ci master stash')

            }

            task(type:'npm', description:'NPM install in AUI',
                command:'install', executable:'Node.js 0.12',
                workingSubDirectory:'aui', environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'npm', description:'NPM install AUI-ADG',
                command:'install --ignore-scripts', executable:'Node.js 0.12',
                workingSubDirectory:'aui-adg')

            task(type:'npm', description:'NPM install bower in AUI-ADG',
                command:'install bower', executable:'Node.js 0.12',
                workingSubDirectory:'aui-adg')

            task(type:'custom', createTaskKey:'com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.bower',
                description:'Bower install in AUI', runtime:'Node.js 0.12',
                command:'install', workingSubDirectory:'aui',
                bowerRuntime:'./node_modules/.bin/bower')

            task(type:'custom', createTaskKey:'com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.bower',
                description:'Bower install in AUI-ADG', runtime:'Node.js 0.12',
                command:'install', workingSubDirectory:'aui-adg',
                bowerRuntime:'./node_modules/.bin/bower')

            task(type:'script', description:'Bower link repositories',
                scriptBody:'''
set -x
set -e

cd aui
./node_modules/.bin/bower link
cd ../aui-adg
./node_modules/.bin/bower link aui
''',
                environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.capability.system.jdk.JDK 1.7}/bin"')

            task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.gulp',
                description:'Plugin install (gulp)',task:'plugin --verbose',
                environmentVariables:'JAVA_HOME=${bamboo.capability.system.jdk.JDK 1.7} PATH="${bamboo.capability.system.builder.mvn3.Maven 3.2}/bin:$PATH"',
                gulpRuntime:'node_modules/gulp/bin/gulp.js',
                runtime:'Node.js 0.12',workingSubDirectory:'aui-adg')

            task(type:'script', description:'Update JIRA with the latest AUI plugin snapshot',
                scriptBody:'''
set -x
set -e

AUI_VERSION=$(echo "console.log(require('./aui-adg/package.json').version)" | node)
sed -i "s#<auiplugin\\.parent\\.version>\\([^<][^<]*\\)</auiplugin\\.parent\\.version>#<auiplugin\\.parent\\.version>$AUI_VERSION</auiplugin\\.parent\\.version>#" pom.xml
echo "AUI Plugin [$AUI_VERSION] has been installed and JIRA is pointing to it."

''')

            task(type:'checkout', description:'Checkout JIRA Baseline images',
                cleanCheckout:'true') {
                repository(name:'JIRA baseline images stash', checkoutDirectory:'jira-baseline-images')

            }

            task(type:'script', description:'Run the webdriver tests',
                scriptBody:'''
set -x
set -e

jira-webdriver-tests/run.sh $*
cd jira-distribution/jira-webdriver-tests-runner/target/test-reports
zip -r report.zip *
exit 0
''',
                argument:'"clean verify -Dmaven.test.func.skip=true -Dmaven.test.unit.skip=true -pl jira-distribution/jira-webdriver-tests-runner -am -Pdistribution -Djira.security.disabled=true -Dwebdriver.browser=firefox -Djava.awt.headless=true -Datlassian.test.suite.numbatches=1 -Datlassian.test.suite.batch=1 -Djira.minify.skip=true -Datlassian.dev.mode=false -Datlassian.test.suite.includes=VISUAL_REGRESSION -Dfunc.mode.plugins -Dreference.plugins"',
                environmentVariables:'MAVEN_OPTS="-Xmx768m -Xms128m -XX:MaxPermSize=256m" M2_HOME="${bamboo.capability.system.builder.mvn3.Maven 3.0}" JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.8}"')


            task(type:'script', description:'Create pull request to baseline images',
                final:'true', script:'bin/add-screenshots-to-pull-request.sh',
                argument:'--baseline.repo.url ssh://git@stash.atlassian.com:7997/jira/jira-baseline-images.git --baseline.repo jira-baseline-images --branch.name ${bamboo.aui.jira.visreg.branch.name} --stash.base.url https://stash.atlassian.com --stash.username ${bamboo.aui.stash.username} --stash.password ${bamboo.aui.stash.password} --stash.project jira',
                environmentVariables:'BUILDKEY="${bamboo.buildResultKey}" JOBKEY="${bamboo.shortJobKey}" BUILDURL="${bamboo.buildResultsUrl}" BAMBOOURL=https://ecosystem-bamboo.internal.atlassian.com BAMBOOUSER=${bamboo.aui.stash.username} BAMBOOPASS=${bamboo.aui.stash.password}')
        }
    }
    dependencies(triggerForBranches:'true')

    permissions() {
        user(name:'buildeng-build-doctor', permissions:'read, write, build, clone, administration')

        anonymous(permissions:'read')

        loggedInUser(permissions:'read')

    }
}
