plan(key:'UT',name:'Unit Tests (Chrome)') {
   project(key:'AUI',name:'AUI')

   repository(name:'AUI')

   variable(key:'aui.plan.branch',value:'master')

   trigger(type:'polling',strategy:'periodically',frequency:'120') {
      repository(name:'AUI-ADG')

      repository(name:'AUI')

   }

   notification(type:'Failed Builds and First Successful',
      recipient:'watchers')

   notification(type:'Change of Build Status',
      recipient:'hipchat',apiKey:'${bamboo.design.platform.hipchat.key.password}',
      notify:'false',room:'Design Platform')

   notification(type:'Failed Builds and First Successful',
      recipient:'committers')

   stage(name:'Default Stage') {
      job(key:'CHROME1',name:'Test with Chrome') {
         requirement(key:'chrome latest',condition:'exists')

         requirement(key:'system.builder.node.Node.js 0.12',condition:'exists')

         requirement(key:'system.jdk.JDK 1.7',condition:'exists')

         requirement(key:'elastic',condition:'equals',value:'false')

         task(type:'script',description:'Clean up VNC',scriptBody:'''
rm -rf /tmp/.X90-lock
rm -rf /tmp/.X11-unix/X90
''')

         task(type:'checkout',description:'Checkout Default Repository')

         task(type:'npm',description:'Clean Cache',command:'cache clean',
            executable:'Node.js 0.12')

         task(type:'npm',description:'Install Dependencies',
             command:'install --no-color',executable:'Node.js 0.12',
             environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.capability.system.jdk.JDK 1.7}/bin"')

         task(type:'script',description:'Setup VNC', scriptBody:'vncserver :90')

          task(type:'npm',description:'npm test',command:'test -- --no-color --browsers Chrome_1024x768',
              executable:'Node.js',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.capability.system.jdk.JDK 1.7}/bin" DISPLAY=:90')

         task(type:'jUnitParser',description:'Karma results',
             final:'true',resultsDirectory:'tests/karma*.xml')

         task(type:'script',description:'Teardown VNC',final:'true',
             scriptBody:'vncserver -kill :90')

      }
   }
   branchMonitoring(enabled:'true',matchingPattern:'\\d.\\d.x|[a-zA-Z0-9].+?-[0-9].+?|(?:issue|epic|feature)/?.*',
      timeOfInactivityInDays:'15',notificationStrategy:'INHERIT',
      remoteJiraBranchLinkingEnabled:'true')

   dependencies(blockingStrategy:'notBlock') {
      childPlan(planKey:'AUI-DOC')

   }
   planMiscellaneous() {
      sandbox(enabled:'true',automaticPromotion:'false')

   }
}
