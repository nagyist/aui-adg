plan(key:'AJS',name:'AUI JIRA SOKE',description:'Runs the JIRA SOKE tests with AUI master') {   
   project(key:'AUI',name:'AUI')

   repository(name:'JIRA soke runner')
   
   repository(name:'AUI-ADG')
   
   repository(name:'AUI')
   
   trigger(type:'cron',description:'Midnight',cronExpression:'0 0 0 ? * *')
   
   trigger(type:'cron',description:'Noon',cronExpression:'0 0 12 ? * *')
   
   trigger(type:'cron',description:'6am',cronExpression:'0 0 6 ? * *')
   
   trigger(type:'cron',description:'6pm',cronExpression:'0 0 18 ? * *')
   
   stage(name:'Build AUI',description:'Builds AUI from master') {      
      job(key:'BAJ',name:'Build AUI JAR') {         
         requirement(key:'os',condition:'equals',value:'Linux')
         
         requirement(key:'system.jdk.JDK 1.7',condition:'exists')
         
         artifactDefinition(name:'AUI Jar',location:'aui-adg/integration/plugin/target',
            pattern:'*.jar',shared:'true')
         
         task(type:'checkout',description:'Checkout AUI ADG') {            
            repository(name:'AUI-ADG',checkoutDirectory:'aui-adg')
            
         }
         task(type:'npm',description:'Build AUI jar',command:'install',
            executable:'Node.js',workingSubDirectory:'aui-adg')
         
         task(type:'script',description:'Remove unwated jar files',
            scriptBody:'''
rm -rf auiplugin-*-sources.jar
rm -rf auiplugin-*-javadoc.jar
''',
            workingSubDirectory:'aui-adg')
         
      }
   }
   stage(name:'Default Stage') {      
      job(key:'JOB1',name:'Soke Tests') {         
         requirement(key:'system.jdk.JDK 1.6',condition:'exists')
         
         requirement(key:'perfeng.mplb1',condition:'equals',value:'speed')
         
         requirement(key:'system.builder.mvn3.MVN3 PerfEng MPLb1',
            condition:'exists')
         
         artifactDefinition(name:'JIRA log',location:'jira',pattern:'**/catalina.out',
            shared:'false')
         
         artifactDefinition(name:'Webdriver Screenshots',location:'target/errors',
            pattern:'*',shared:'false')
         
         artifactDefinition(name:'SOKE jira-stable',location:'target',pattern:'report*.zip',
            shared:'true')
         
         artifactSubscription(name:'AUI Jar',destination:'aui-adg-jar')
         
         task(type:'checkout',description:'Checkout Default Repository (jira-bugfix)')
         
         task(type:'checkout',description:'Checkout SOKE Runner') {            
            repository(name:'JIRA soke runner',checkoutDirectory:'./runner')
            
         }
         task(type:'script',description:'Download and install db, home and test-param files',
            script:'runner/libexec/jsoke',argument:'setup jira-soke-193k-agile-lexo')
         
         task(type:'maven3',description:'Build soke and download dependencies',
            goal:'clean package -B',mavenExecutable:'MVN3 PerfEng MPLb1',
            buildJdk:'JDK 1.6')
         
         task(type:'script',description:'Start JIRA',script:'runner/libexec/jsoke',
            argument:'start',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.6}" AUI_SOKE_JAR_LOCATION="${bamboo.build.working.directory}/aui-adg-jar/"')
         
         task(type:'script',description:'Set base URL',script:'runner/bin/baseurl.sh')
         
         task(type:'script',description:'Invoke SOKE Client',
            script:'../runner/libexec/jsoke',argument:'run-remote',
            workingSubDirectory:'target')
         
         task(type:'script',description:'Clean Unicode characters from the results',
            script:'runner/bin/fixnonasciidata.sh')
         
         task(type:'script',description:'Delayed reporting of soke exit status',
            scriptBody:'''
test -f report-standard.zip && exit 0
echo "Could not find the report zip for Koto!" >&2
exit 1

# Maybe do this instead?
rc=127
test -f soke-rc.txt && rc=`cat soke-rc.txt`
test -z "$rc" && rc=126
test -d errors && rc=125
exit $rc
''',
            workingSubDirectory:'target')
         
      }
   }
   stage(name:'Publish Results') {      
      job(key:'PR',name:'Publish Results',description:'Publish results to Koto') {         
         artifactSubscription(name:'SOKE jira-stable',destination:'target')
         
         task(type:'custom',createTaskKey:'io.koto.bamboo-koto-plugin:publishkoto',
            description:'Publish results (jira-bugfix)',
            productid:'aui-jira-master',artefact:'target/report-standard.zip',
            timeout:'600')
         
      }
   }
   dependencies(triggerForBranches:'true')
   
}
