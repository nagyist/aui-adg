plan(key:'DOC',name:'Documentation staging') {
   project(key:'AUI',name:'AUI')

   repository(name:'AUI-ADG')

   repository(name:'AUI')

   variable(key:'aui.plan.branch',value:'master')

   trigger(type:'polling',strategy:'periodically',frequency:'60') {
      repository(name:'AUI')

      repository(name:'AUI-ADG')

   }
   stage(name:'Build AUI Documentation') {
      job(key:'JOB1',name:'Build AUI Documentation',description:'Builds the AUI Sandbox and the AUI Documentation') {
         requirement(key:'system.builder.node.Node.js 0.12',condition:'exists')

         requirement(key:'os',condition:'equals',value:'Linux')

         requirement(key:'system.jdk.JDK 1.7',condition:'exists')

         artifactDefinition(name:'AUI Docs',location:'aui-adg/.tmp/docs/dist',
            pattern:'**/**',shared:'true')

         artifactDefinition(name:'AUI Version',pattern:'version',shared:'true')

         task(type:'script',description:'Install mvnvm',scriptBody:'''
set -x
set -e

mkdir -p ${bamboo.build.working.directory}/bin
curl -s https://bitbucket.org/mjensen/mvnvm/raw/mvnvm-1.0.3/mvn > ${bamboo.build.working.directory}/bin/mvn
chmod 0755 ${bamboo.build.working.directory}/bin/mvn
''',
             environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

          task(type:'checkout',description:'Checkout aui',
              cleanCheckout:'true') {
              repository(name:'AUI',checkoutDirectory:'aui')

          }
          task(type:'checkout',description:'Checkout aui-adg') {
              repository(name:'AUI-ADG',checkoutDirectory:'aui-adg')

          }
          task(type:'npm',description:'npm install bower in aui',
              command:'install bower --no-color',executable:'Node.js',
              workingSubDirectory:'aui',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

          task(type:'npm',description:'npm install bower in aui-adg',
              command:'install bower --no-color',executable:'Node.js',
              workingSubDirectory:'aui-adg',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

          task(type:'script',description:'bower link aui in aui-adg',
              scriptBody:'''
set -x
set -e

cd aui
./node_modules/.bin/bower link --no-color
cd ../aui-adg
./node_modules/.bin/bower link aui --no-color
''',
              environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

          task(type:'npm',description:'npm install in aui',
              command:'install --no-color',executable:'Node.js',
              workingSubDirectory:'aui',environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

          task(type:'npm',description:'npm install in aui-adg',
              command:'install --ignore-scripts --no-color',
              executable:'Node.js',workingSubDirectory:'aui-adg',
              environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"')

          task(type:'script',description:'Build Docs',scriptBody:'''
set -x
set -e

npm run docs/build --no-color
cat package.json | grep '"version":' | sed 's/.*"version": "\\([^"]*\\)".*/\\1/' > ../version
''',
              environmentVariables:'JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.7}" PATH="${bamboo.build.working.directory}/bin:${bamboo.capability.system.jdk.JDK 1.7}/bin"',
              workingSubDirectory:'aui-adg')

      }
   }
   dependencies(blockingStrategy:'notBlock',triggerOnlyAfterAllStagesGreen:'true',
      triggerForBranches:'true')

}

deployment(name:'Deploy documentation (to staging for CI)',planKey:'AUI-DOC') {
   versioning(version:'release-18',autoIncrementNumber:'true')

   environment(name:'Staging') {
      trigger(type:'afterSuccessfulPlan')

      task(type:'addRequirement') {
         requirement(key:'os',condition:'equals',value:'Linux')

      }
      task(type:'cleanWorkingDirectory')

      task(type:'artifactDownload',description:'Download release contents',
         planKey:'AUI-DOC') {
         artifact(name:'all artifacts',localPath:'docs')

      }
      task(type:'script',description:'Upload docs',scriptBody:'''
set -x
set -e

SERVER=docs-app.stg.internal.atlassian.com
URL=/var/www/domains/docs.atlassian.com/aui
VERSION=`cat docs/version`
rsync -e \'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no\' -rl --delete docs/ uploads@$SERVER:$URL/$VERSION/
rsync -e \'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no\' -rl --delete docs/ uploads@$SERVER:$URL/latest/
''')

   }
   environment(name:'Staging - Stable') {
      trigger(type:'afterSuccessfulPlan')

      task(type:'addRequirement') {
         requirement(key:'os',condition:'equals',value:'Linux')
      }

      task(type:'cleanWorkingDirectory')

      task(type:'artifactDownload',description:'Download release contents',
         planKey:'AUI-DOC') {
         artifact(name:'all artifacts',localPath:'docs')

      }
      task(type:'script',description:'Upload stable docs',
         scriptBody:'''
set -x
set -e

SERVER=docs-app.stg.internal.atlassian.com
URL=/var/www/domains/docs.atlassian.com/aui
VERSION=`cat docs/version`
rsync -e \'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no\' -rl --delete docs/ uploads@$SERVER:$URL/$VERSION/

''')

   }
}
