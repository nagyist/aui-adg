'use strict';

var docsBuild = require('./docs/build');
var docsServer = require('../../bower_components/aui/build/gulp/docs/server');
var mac = require('mac');

module.exports = mac.series(
    docsBuild,
    docsServer
);
