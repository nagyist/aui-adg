'use strict';

var gulp = require('gulp');
var mac = require('../lib/mac');
var pluginDeps = require('./plugin/deps');
var pluginMavenInstall = require('./plugin/maven-install');

module.exports = mac.series(
    pluginDeps,
    pluginMavenInstall
);
