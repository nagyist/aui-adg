'use strict';

var mac = require('../lib/mac');
var auiFlatappServer = require('../../bower_components/aui/build/gulp/flatapp/server');
var auiFlatappBuild = require('../../bower_components/aui/build/gulp/flatapp/build');
var flatappCopy = require('./flatapp/copy');
var auiI18n = require('../../bower_components/aui/build/gulp/i18n');
var auiAdgDist = require('./dist');

module.exports = mac.series(
    auiAdgDist,
    auiI18n,
    flatappCopy,
    auiFlatappBuild,
    auiFlatappServer
);
