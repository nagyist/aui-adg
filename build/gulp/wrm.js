'use strict';

var galv = require('../lib/galvatron');
var path = require('path');

var auiAdgPath = path.resolve(path.join(__dirname, '..', '..'));
var auiAdgJsPath = path.join(auiAdgPath, 'src', 'js');
var root = path.join(__dirname, '..', '..');

function pathToKey (file) {
    file = file.replace('.js', '');
    file = file.replace(/[^a-z0-9\-]/gi, '-');
    file = file.replace(/-+/g, '-');
    return 'internal-' + file;
}

function pathToName (file) {
    return pathToKey(file) + '.js';
}

var declaredKeys = [];

function xml (filePath, deps) {
    var out = '';
    var key = pathToKey(filePath);

    out += '<web-resource key="' + key + '" name="' + key + '">\n';
    out += '    <transformation extension="js">\n';
    out += '        <transformer key="jsI18n" />\n';
    out += '    </transformation>\n';

    deps.forEach(function (dep) {
        out += '    <dependency>com.atlassian.auiplugin:' + pathToKey(dep) + '</dependency>\n';
    });

    out += '    <resource type="download" name="' + pathToName(filePath) + '" location="' + filePath + '" />\n';
    out += '</web-resource>';

    return out;
}

module.exports = function () {
    galv.tracer.trace(path.join(auiAdgJsPath, 'aui*.js')).forEach(function (file) {
        var filePath = path.relative(auiAdgPath, file.path);
        var deps = file.imports.map(function (imp) {
            return path.relative(auiAdgPath, imp.path);
        });
        var key = pathToKey(filePath);

        if (declaredKeys.indexOf(key) !== -1) {
            return;
        }

        declaredKeys.push(key);

        console.log(xml(filePath, deps));
        console.log('');
    });
};
