'use strict';

var i18n = require('./plugin/i18n');
var mac = require('../lib/mac');
var plugin = require('./plugin');
var refappRun = require('./refapp/run');

module.exports = mac.series(
    plugin,
    refappRun
);
