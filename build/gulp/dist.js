'use strict';

var dist = require('../../bower_components/aui/build/gulp/dist');
var gulp = require('gulp');
var mac = require('mac');
var path = require('path');

var auiSource = 'bower_components/aui/src';

module.exports = mac.series(
    dist,
    function copyFontFiles () {
        return gulp
            .src('src/less/fonts/**')
            .pipe(gulp.dest('dist/aui/css/fonts'));
    },
    function auiMainImages () {
        return gulp.src(path.join(auiSource, 'less/images/**/*'))
            .pipe(gulp.dest('dist/aui/css'));
    },
    function auiSelect2Images () {
        return gulp
            .src([
                path.join(auiSource, 'css-vendor/jquery/plugins/*.png'),
                path.join(auiSource, 'css-vendor/jquery/plugins/*.gif')
            ])
            .pipe(gulp.dest('dist/aui/css'));
    },
    function copyAuiAdgLicense () {
        return gulp
            .src('LICENSE')
            .pipe(gulp.dest('dist'));
    }
);
