'use strict';

var mac = require('../../lib/mac');
var copyAndWatch = require('../../../bower_components/aui/build/lib/copy-and-watch');

var copyAui = mac.parallel(
    copyAndWatch('dist/aui/**', '.tmp/flatapp/src/static/common'),
    copyAndWatch('bower_components/aui/tests/flatapp/src/soy/**/*.soy', '.tmp/flatapp/src/soy'),
    copyAndWatch('bower_components/aui/tests/flatapp/src/static/**', '.tmp/flatapp/src/static'),
    copyAndWatch('bower_components/aui/tests/test-pages/pages/**/*.soy', '.tmp/flatapp/src/soy/pages'),
    copyAndWatch(['bower_components/aui/tests/test-pages/pages/**', '!**/*.soy'], '.tmp/flatapp/src/static/pages'),
    copyAndWatch('bower_components/aui/tests/test-pages/common/**/*.soy', '.tmp/flatapp/src/soy/dependencies'),
    copyAndWatch('bower_components/aui/tests/test-pages/common/**', '.tmp/flatapp/src/static/common')
);

var copyAuiAdg = mac.parallel(
    copyAndWatch('tests/flatapp/src/soy/**/*.soy', '.tmp/flatapp/src/soy'),
    copyAndWatch('tests/test-pages/pages/**/*.soy', '.tmp/flatapp/src/soy/pages'),
    copyAndWatch(['tests/test-pages/pages/**', '!**/*.soy'], '.tmp/flatapp/src/static/pages')
);

module.exports = mac.series(
    copyAui,
    copyAuiAdg
);
