'use strict';

var commander = require('commander');
var mavenStdoutFilter = require('../../tasks/lib/maven-stdout-filter');
var path = require('path');

commander
    .option('-d, --debug', 'Wether or not to run Refapp in debug mode.')
    .option('-j, --jquery [version]', 'The version of jquery to use. Defaults to 1.8.3.')
    .option('-v, --verbose', 'Turn on verbose logging.')
    .parse(process.argv);

module.exports = function (done) {
    var INTEGRATION_PATH = 'integration/refapp';
    var proc = require('child_process');
    var debug = commander.debug ? 'debug' : 'run';
    var args = [
        'amps:' + debug,
        '-DskipTests',
        '-DskipAllPrompts=true',
        '-Djquery.version=' + (commander.jquery || '1.8.3'),
        '-Daui.location=' + path.resolve(path.join('bower_components', 'aui')),
        '-Daui-adg.test.page.location=' + path.resolve(path.join('tests', 'test-pages')),
        '-Daui.test.page.location=' + path.resolve(path.join('bower_components', 'aui', 'tests', 'test-pages'))
    ];

    if (commander.verbose) {
        console.log('mvn ' + args.join(' '));
    }

    var cmd = proc.spawn('mvn', args, {
        cwd: INTEGRATION_PATH
    });

    cmd.stdout.on('data', mavenStdoutFilter(commander.verbose));
    cmd.stdout.on('data', function(data) {
        data = data.toString();

        if (data.indexOf('refapp started successfully') !== -1) {
            console.log(data);
        } else if (data.indexOf('JDWP exit error') !== -1) {
            console.error(data);
        }
    });

    cmd.on('close', done);
};
