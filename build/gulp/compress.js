var commander = require('commander'),
    del = require('del'),
    gulp = require('gulp'),
    path = require('path'),
    pkg = require('../../package.json'),
    zip = require('gulp-zip'),
    docsJs = require('../../bower_components/aui/build/gulp/docs/js'),
    docsLess = require('../../bower_components/aui/build/gulp/docs/less'),
    mac = require('mac');

commander
    .option('-d, --dist-path <path>', 'Path to the AUI dist.')
    .parse(process.argv);

function compressDist () {
    var distPath = path.resolve(commander.distPath);
    return gulp.src(path.join(distPath, '**'))
        .pipe(zip('aui-flat-pack-' + pkg.version + '.zip'))
        .pipe(gulp.dest(distPath));
}

module.exports = function () {
    return mac.series(
        docsJs,
        docsLess,
        compressDist()
    );
};
