'use strict';

var docsAssets = require('../../../bower_components/aui/build/gulp/docs/assets');
var docsClean = require('../../../bower_components/aui/build/gulp/docs/clean');
var docsJs = require('../../../bower_components/aui/build/gulp/docs/js');
var docsLess = require('../../../bower_components/aui/build/gulp/docs/less');
var docsLessAssets = require('../../../bower_components/aui/build/gulp/docs/less-assets');
var docsMetalsmith = require('../../../bower_components/aui/build/gulp/docs/metalsmith');
var mac = require('mac');

module.exports = mac.series(
    docsClean,
    docsLessAssets.bind({
        paths: [
            'bower_components/aui/src/less/{fonts,images}/**/*',
            'src/less/{fonts,images}/**/*'
        ]
    }),
    docsAssets.bind({
        paths: [
            'bower_components/aui/docs/**/*',
            'docs/**/*',
        ]
    }),
    docsMetalsmith,
    mac.parallel(
        docsJs,
        docsLess
    )
);
