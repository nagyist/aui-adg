'use strict';

var gulp = require('gulp');
var mac = require('../../lib/mac');
var pluginJs = require('./js');
var pluginLess = require('./less');
var pluginI18n = require('./i18n');

module.exports = mac.series(
    pluginI18n,
    pluginJs,
    pluginLess
);
