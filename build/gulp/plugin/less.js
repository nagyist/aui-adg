'use strict';

var gulp = require('gulp');
var mac = require('../../lib/mac');

function copyAuiLess () {
    return gulp
        .src(['bower_components/aui/src/less/**/*', '!bower_components/aui/src/less/imports/**', '!bower_components/aui/src/less/batch/**'])
        .pipe(gulp.dest('.tmp/plugin/src/less'));
}

function copyAuiAdgLess () {
    return gulp
        .src(['src/less/**/*', '!src/less/imports/**', '!src/less/batch/**'])
        .pipe(gulp.dest('.tmp/plugin/src/less'));
}

function copyAuiLessTheme () { //aui-adg less theme depends on the AUI theme
    return gulp
        .src('bower_components/aui/src/less/imports/aui-theme/**/*')
        .pipe(gulp.dest('.tmp/plugin/src/less/imports/aui-theme'));
}

function copyAuiAdgLessTheme () {
    return gulp
        .src('src/less/imports/adg-theme/**/*')
        .pipe(gulp.dest('.tmp/plugin/src/less/imports/adg-theme'));
}

function cherryPickGlobalImports () {
    return gulp
        .src(['src/less/imports/global.less', 'bower_components/aui/src/less/imports/mixins.less'])
        .pipe(gulp.dest('.tmp/plugin/src/less/imports'));
}

module.exports = mac.series(
    copyAuiLess,
    copyAuiAdgLess,
    copyAuiLessTheme,
    copyAuiAdgLessTheme,
    cherryPickGlobalImports
);
