'use strict';

var galv = require('../../lib/galvatron');
var fs = require('fs-extra');
var minimatch = require('minimatch');
var nodePath = require('path');

module.exports = function (done) {
    galv.tracer.trace('src/js/aui*.js').forEach(function (file) {
        var path = file.path;

        // Ignore batch files.
        if (minimatch(path, '**/src/js/aui*.js')) {
            return;
        }

        // Don't need the soy JS files.
        if (minimatch(path, '**/.tmp/compiled-soy/**')) {
            return;
        }

        // Make relative to the current working directory.
        path = nodePath.relative(process.cwd(), path);

        console.log('OUTPUT ' + path);
        fs.outputFile('.tmp/plugin/' + path, file.post);
    });
    done();
};
