'use strict';

var gulp = require('gulp');
var gulpRename = require('gulp-rename');
var mapStream = require('map-stream');
var vinylTransform = require('vinyl-transform');

function keysToProperties (keys) {
    return function (key) {
        return key + '=' + keys[key];
    };
}

function streamToProperties () {
    return vinylTransform(function (file) {
        var keys = require(file);
        var keyToProperty = keysToProperties(keys);
        return mapStream(function (data, next) {
            next(null, Object.keys(keys).map(keyToProperty).join('\n'));
        });
    });
}

module.exports = function () {
    return gulp.src('bower_components/aui/lib/js/aui/internal/i18n/keys.js')
        .pipe(streamToProperties())
        .pipe(gulpRename({ basename: 'aui', extname: '.properties' }))
        .pipe(gulp.dest('.tmp/plugin/i18n'));
};
