var commander = require('commander');
var mavenStdoutFilter = require('../../tasks/lib/maven-stdout-filter');
var path = require('path');
var spawn = require('child_process').spawn;


module.exports = function(done) {
    var INTEGRATION_PATH = 'integration';
    var mavenArguments = [
        'clean',
        'install',
        '-DskipTests',
        '-DskipAllPrompts=true',
        '-Djquery.version=' + (commander.jquery || '1.8.3'),
        '-Daui.location=' + path.resolve(path.join('bower_components', 'aui')),
        '-Daui-adg.test.page.location=' + path.resolve(path.join('tests', 'test-pages')),
        '-Daui.test.page.location=' + path.resolve(path.join('bower_components', 'aui', 'tests', 'test-pages'))
    ];

    mavenArguments.push(['-X']);

    console.log('Executing mvn ' + mavenArguments.join(' '));

    var cmd = spawn('mvn', mavenArguments, {
        cwd: INTEGRATION_PATH
    });

    cmd.stdout.on('data', mavenStdoutFilter(commander.verbose));
    cmd.stderr.on('data', mavenStdoutFilter(commander.verbose));

    cmd.on('close', function(code) {
        done(code);
    });
};
