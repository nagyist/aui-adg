var pkg = require('../../package.json');
var gulp = require('gulp');
var minimist = require('minimist');
var gzip = require('gulp-gzip');
var s3 = require('gulp-s3-upload');
var filter = require('gulp-filter');
var path = require('path');
var chalk = require('chalk');
var merge = require('merge-stream');

module.exports = function() {
    var DEFAULT_BUCKET = 'aui-origin.herokuapp.com';
    var DEFAULT_DIRECTORY = 'dist/aui';
    var options = minimist(process.argv);

    var directory = typeof options.directory === 'string' ? options.directory : DEFAULT_DIRECTORY;
    var directoryGlob = directory + '/**/**';
    var bucket = options.bucket || DEFAULT_BUCKET;
    var version = options.releaseVersion || pkg.version;
    var awsId = process.env.AWS_ACCESS_KEY_ID;
    var awsKey = process.env.AWS_SECRET_ACCESS_KEY;
    console.log('Uploading', chalk.blue(directory), 'to', chalk.red(bucket), 'with version:', chalk.green(version));

    merge(
        gulp.src(directoryGlob)
            .pipe(s3({key: awsId, secret: awsKey})({
                bucket: bucket,
                keyTransform: toVersion(version)
            })),
        gulp.src(directoryGlob)
            .pipe(gzip())
            .pipe(s3({key: awsId, secret: awsKey})({
                bucket: bucket,
                keyTransform: toVersion(version),
                mimeTypeLookup: function(originalFilepath) {
                    return originalFilepath.replace('.gz', ''); //ignore gzip
                },
                ContentEncoding: 'gzip'
            }))
    );
};

function toVersion(version){
    return function(relative_filename) {
        var newPath = path.join('aui-adg', version, relative_filename);
        return newPath;
    }
}
