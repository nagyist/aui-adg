'use strict';

var galv = require('../../bower_components/aui/build/lib/galvatron');
var path = require('path');

galv.fs.map({
    // We must override AUI's path for these because if it is resolved via a
    // bower link symlink then it does not resolve relative to the symlink
    // inside of AUI.
    jquery: path.join('bower_components', 'aui', 'src', 'js', 'aui', 'jquery.js'),
    underscore: path.join('bower_components', 'aui', 'src', 'js-vendor', 'underscorejs', 'underscore.js')
});

module.exports = galv;
