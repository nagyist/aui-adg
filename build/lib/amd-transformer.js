'use strict';

var amdNoop = require('./amd-noop');

// No-op AMD because it causes problems when you don't put anonymouse defines
// through r.js and don't have control over when your code is loaded.
module.exports = function (file, code) {
  if (file.path.indexOf('aui/internal/amdify') === -1) {
    return amdNoop(code);
  }

  return code;
};
