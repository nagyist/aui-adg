'use strict';

var fs = require('fs');

function noop (str) {
    return str.replace(/\s*define\(/g, '\n(function () {})(');
}

noop.file = function noopAmd (file) {
    fs.writeFileSync(file, noop(fs.readFileSync(file).toString()));
};

module.exports = noop;
