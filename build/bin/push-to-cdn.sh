#!/bin/bash
# check vars
[ -z "$1" ] && [ -z "$2" ] && echo "Usage: push-to-cdn-prepare.sh.sh [release-version] [s3-bucket]" && exit 1

# variables
RELEASE_VERSION=$1
S3_BUCKET=$2

# go to the directory push-to-cdn-prepare.sh left us in
cd .uploadToCdn

# Requires some setup:
aws --profile auicdn s3 cp aui s3://$S3_BUCKET/aui-adg/$RELEASE_VERSION --recursive --exclude "*.gz" --cache-control "public, max-age=86400"
aws --profile auicdn s3 cp aui s3://$S3_BUCKET/aui-adg/$RELEASE_VERSION --recursive --exclude "*" --include "*.gz" --cache-control "public, max-age=86400"  --content-encoding="gzip"

# clean up
cd ..
rm -rf .uploadToCdn
