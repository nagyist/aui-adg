#!/bin/sh
# check vars
[ -z "$1" ] && echo "Usage: push-to-cdn-prepare.sh.sh [release-version]" && exit 1

# variables
RELEASE_VERSION=$1

# Clone Dist
git clone git@bitbucket.org:atlassian/aui-adg-dist.git .uploadToCdn
cd .uploadToCdn
git checkout tags/$RELEASE_VERSION

#Can't use -k since bamboo doesn't have gzip 1.6+
cp -R aui aui-temp
gzip -9 -r aui
cp -R aui-temp/* aui/
rm -rf aui-temp