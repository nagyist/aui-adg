#!/bin/bash
set -x
set -e

BRANCH=$1
RELEASE_VERSION=$2
NEXT_VERSION=$3
if [ -z "${BRANCH}" -o -z "${RELEASE_VERSION}" -o -z "${NEXT_VERSION}" ]; then
  echo "Usage: $(basename ${0}) [branch] [release-version] [next-version]"
  exit 1
fi

basedir=$(pwd)

##
# Deploy dist to Maven.
#
cd ${basedir}/aui-adg
./node_modules/.bin/gulp compress --no-color --dist-path dist
cd ${basedir}/aui-adg/integration
distzip="${basedir}/aui-adg/dist/aui-flat-pack-${RELEASE_VERSION}.zip"
mvn deploy:deploy-file --batch-mode \
                       -DgroupId=com.atlassian.aui -DartifactId=aui-flat-pack -Dversion="${RELEASE_VERSION}" \
                       -Dpackaging=zip -Dfile=${distzip} \
                       -DrepositoryId=atlassian-public -Durl=https://maven.atlassian.com/public \
                       -DskipTests
rm ${distzip}

##
# Deploy plugin to Maven.
#
cd ${basedir}/aui-adg
./node_modules/.bin/gulp plugin/deps --no-color
cd ${basedir}/aui-adg/integration
mvn clean deploy --batch-mode -DskipTests

##
# Update dist repo.
#
cd ${basedir}
git clone git@bitbucket.org:atlassian/aui-adg-dist.git
rm -rf ${basedir}/aui-adg-dist/*
rm -rf ${basedir}/aui-adg-dist/.gitignore

cd ${basedir}/aui-adg-dist
cp -rf ${basedir}/aui-adg/dist/* .
git add .
git commit -am "Release ${RELEASE_VERSION}."
git tag -a ${RELEASE_VERSION} -m "${RELEASE_VERSION}"
git push origin ${RELEASE_VERSION}

cd ${basedir}
rm -rf ${basedir}/aui-adg-dist

##
# Generate lib and publish for npm
#
cd ${basedir}/aui-adg
npm publish . --registry=https://npm-private.atlassian.io

##
# Bumps the version in all modules and pushes.
#
cd ${basedir}/aui-adg
git reset --hard HEAD^^  # Prior to "npm version" auto-generated commit and mvn versions commit.

cd ${basedir}/aui-adg/integration
# mvn versions:set exits with 0 even if the version is already set to ${NEXT_VERSION}.
mvn versions:set --batch-mode -DnewVersion=${NEXT_VERSION} -DgenerateBackupPoms=false
cd ${basedir}/aui-adg
ret=0
git commit -am "Bump maven version to ${NEXT_VERSION}." || ret=$?

ret=0
npm version ${NEXT_VERSION} || ret=$?
if [ ${ret} -eq 0 ]; then
  git tag -d v${NEXT_VERSION}
  git push git@bitbucket.org:atlassian/aui-adg.git ${BRANCH}
fi
