#!/bin/bash
set -x
set -e

BRANCH=$1
RELEASE_VERSION=$2
if [ -z "${BRANCH}" -o -z "${RELEASE_VERSION}" ]; then
  echo "Usage: $(basename ${0}) [branch] [release-version]"
  exit 1
fi

basedir=$(pwd)

# Set Maven version.
cd ${basedir}/aui-adg/integration
mvn versions:set --batch-mode -DnewVersion=${RELEASE_VERSION} -DgenerateBackupPoms=false
cd ${basedir}/aui-adg
git commit -am "Prepare ${RELEASE_VERSION}."

# Set NPM version.
npm version ${RELEASE_VERSION}
# Rename the tag NPM creates (since we don't want the "v" prefix).
git tag -d v${RELEASE_VERSION}
git tag -a ${RELEASE_VERSION} -m "Release ${RELEASE_VERSION}."
git push git@bitbucket.org:atlassian/aui-adg.git ${RELEASE_VERSION}

./node_modules/.bin/gulp dist --no-color
