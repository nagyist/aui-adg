#!/bin/bash
PATTERN=$1

LAST_MODIFEID_TIMESTAMPS=''

for ((i=2;i<=$#;i++))
do
  pushd ${!i} > /dev/null

  OUTPUT=$(du -a |
  grep ${PATTERN} |
  awk '{print $2}' |
  xargs stat -f  "%Sm" -t "%s") #Get the last modified date, and ignore errors

  LAST_MODIFEID_TIMESTAMPS=${OUTPUT}' '${LAST_MODIFEID_TIMESTAMPS}
  popd > /dev/null
done

echo ${LAST_MODIFEID_TIMESTAMPS} |
tr ' ' '\n' |
sort -r | #Sort from biggest to smallest
sed -n 1p #Get the biggest