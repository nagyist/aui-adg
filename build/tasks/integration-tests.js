var path = require('path');
var proc = require('child_process');

var INTEGRATION_PATH = 'integration/refapp';

var auiLocation = path.resolve('bower_components/aui/');
var testPageSource = 'tests/test-pages';
var auiTestPageSource = auiLocation + '/tests/test-pages';

module.exports = function(args, paths, cb) {
    var cmd = proc.spawn('mvn', [
        'verify',
        '-DskipAllPrompts=true',
        '-Djquery.version=' + (args.jquery || '1.8.3'),
        '-Daui-adg.test.page.location=' + path.resolve(testPageSource),
        '-Daui.test.page.location=' + path.resolve(auiTestPageSource)
    ], {
        cwd: INTEGRATION_PATH
    });

    cmd.stdout.on('data', function(data) {
        console.log(data.toString());
    });

    cmd.on('close', function(code) {
        cb(code);
    });
};
