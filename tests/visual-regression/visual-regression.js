module.exports = function(casper) {
    return {
        run: function (url, tests) {
            casper.start(url);

            for (var page in tests) {
                (function(page, selector) {
                    casper.thenOpen(url + '/' + page).then(function() {
                        phantomcss.screenshot(selector, page + '-' + selector);
                    });
                })(page, tests[page]);
            }
        }
    };
};
