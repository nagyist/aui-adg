#! /usr/bin/env node

var request = require('request');
var path = require('path');
var mime = require('mime');
var minimist = require('minimist');
var package = require('../package.json');
var Promise = require('es6-promise').Promise;
var chalk = require('chalk');

//TODO: Once we have ES6, programatically generate EXPECTED_FILES https://ecosystem.atlassian.net/browse/DP-369
var EXPECTED_FILES = [
    'css/aui-experimental.css',
    'css/aui-experimental.css.gz',
    'css/aui-experimental.min.css',
    'css/aui-experimental.min.css.gz',
    'css/aui-iconfonts.css',
    'css/aui-iconfonts.css.gz',
    'css/aui-iconfonts.min.css',
    'css/aui-iconfonts.min.css.gz',
    'css/aui.css',
    'css/aui.css.gz',
    'css/aui.min.css',
    'css/aui.min.css.gz',
    'css/bg-grippy.png',
    'css/bg-grippy.png.gz',
    'css/fonts/atlassian-icons.eot',
    'css/fonts/atlassian-icons.eot.gz',
    'css/fonts/atlassian-icons.svg',
    'css/fonts/atlassian-icons.svg.gz',
    'css/fonts/atlassian-icons.ttf',
    'css/fonts/atlassian-icons.ttf.gz',
    'css/fonts/atlassian-icons.woff',
    'css/fonts/atlassian-icons.woff.gz',
    'css/icons/aui-icon-close.png',
    'css/icons/aui-icon-close.png.gz',
    'css/icons/aui-icon-tools.gif',
    'css/icons/aui-icon-tools.gif.gz',
    'css/icons/core/icon-dropdown-active-d.png',
    'css/icons/core/icon-dropdown-active-d.png.gz',
    'css/icons/core/icon-dropdown-active.png',
    'css/icons/core/icon-dropdown-active.png.gz',
    'css/icons/core/icon-dropdown-d.png',
    'css/icons/core/icon-dropdown-d.png.gz',
    'css/icons/core/icon-dropdown.png',
    'css/icons/core/icon-dropdown.png.gz',
    'css/icons/core/icon-maximize-d.png',
    'css/icons/core/icon-maximize-d.png.gz',
    'css/icons/core/icon-maximize.png',
    'css/icons/core/icon-maximize.png.gz',
    'css/icons/core/icon-minimize-d.png',
    'css/icons/core/icon-minimize-d.png.gz',
    'css/icons/core/icon-minimize.png',
    'css/icons/core/icon-minimize.png.gz',
    'css/icons/core/icon-move-d.png',
    'css/icons/core/icon-move-d.png.gz',
    'css/icons/core/icon-move.png',
    'css/icons/core/icon-move.png.gz',
    'css/icons/core/icon-search.png',
    'css/icons/core/icon-search.png.gz',
    'css/icons/forms/icon-date.png',
    'css/icons/forms/icon-date.png.gz',
    'css/icons/forms/icon-help.png',
    'css/icons/forms/icon-help.png.gz',
    'css/icons/forms/icon-range.png',
    'css/icons/forms/icon-range.png.gz',
    'css/icons/forms/icon-required.png',
    'css/icons/forms/icon-required.png.gz',
    'css/icons/forms/icon-users.png',
    'css/icons/forms/icon-users.png.gz',
    'css/icons/messages/icon-close-inverted.png',
    'css/icons/messages/icon-close-inverted.png.gz',
    'css/icons/messages/icon-close.png',
    'css/icons/messages/icon-close.png.gz',
    'css/images/arrow.png',
    'css/images/arrow.png.gz',
    'css/images/atlassian-logo-grey.min.svg',
    'css/images/atlassian-logo-grey.min.svg.gz',
    'css/images/atlassian-logo-grey.svg',
    'css/images/atlassian-logo-grey.svg.gz',
    'css/images/atlassian-logo.min.svg',
    'css/images/atlassian-logo.min.svg.gz',
    'css/images/atlassian-logo.svg',
    'css/images/atlassian-logo.svg.gz',
    'css/images/fav_off_16.png',
    'css/images/fav_off_16.png.gz',
    'css/images/fav_on_16.png',
    'css/images/fav_on_16.png.gz',
    'css/images/forms/icons_form.gif',
    'css/images/forms/icons_form.gif.gz',
    'css/images/icons/aui-message-icon-sprite.png',
    'css/images/icons/aui-message-icon-sprite.png.gz',
    'css/images/icons/messages/icon-close-inverted.png',
    'css/images/icons/messages/icon-close-inverted.png.gz',
    'css/images/icons/messages/icon-close.png',
    'css/images/icons/messages/icon-close.png.gz',
    'css/images/icons/messages/icon-error-white.png',
    'css/images/icons/messages/icon-error-white.png.gz',
    'css/images/icons/messages/icon-error.png',
    'css/images/icons/messages/icon-error.png.gz',
    'css/images/icons/messages/icon-generic.png',
    'css/images/icons/messages/icon-generic.png.gz',
    'css/images/icons/messages/icon-hint.png',
    'css/images/icons/messages/icon-hint.png.gz',
    'css/images/icons/messages/icon-info.png',
    'css/images/icons/messages/icon-info.png.gz',
    'css/images/icons/messages/icon-success.png',
    'css/images/icons/messages/icon-success.png.gz',
    'css/images/icons/messages/icon-warning.png',
    'css/images/icons/messages/icon-warning.png.gz',
    'css/images/wait.gif',
    'css/images/wait.gif.gz',
    'css/select2-spinner.gif',
    'css/select2-spinner.gif.gz',
    'css/select2.png',
    'css/select2.png.gz',
    'css/select2x2.png',
    'css/select2x2.png.gz',
    'css/toolbar/aui-toolbar-24px.png',
    'css/toolbar/aui-toolbar-24px.png.gz',
    'js/aui-css-deprecations.js',
    'js/aui-css-deprecations.js.gz',
    'js/aui-css-deprecations.min.js',
    'js/aui-css-deprecations.min.js.gz',
    'js/aui-datepicker.js',
    'js/aui-datepicker.js.gz',
    'js/aui-datepicker.min.js',
    'js/aui-datepicker.min.js.gz',
    'js/aui-experimental.js',
    'js/aui-experimental.js.gz',
    'js/aui-experimental.min.js',
    'js/aui-experimental.min.js.gz',
    'js/aui-soy.js',
    'js/aui-soy.js.gz',
    'js/aui-soy.min.js',
    'js/aui-soy.min.js.gz',
    'js/aui.js',
    'js/aui.js.gz',
    'js/aui.min.js',
    'js/aui.min.js.gz'
];
var BASE_URL = 'https://s3.amazonaws.com/aui-origin.herokuapp.com/aui-adg/';

var options = minimist(process.argv.slice(2));
var errors = [];
var version = options.cdnVersion || package.version;

testObjects(EXPECTED_FILES);

function testObjects(objects) {
    var promises = [];
    EXPECTED_FILES.forEach(function(object){
        promises.push(detectErrorsWithObject(object));
    });
    Promise.all(promises).then(function(data){
        finalReport(errors);
    });
}

function detectErrorsWithObject(object) {
    var newPromise = new Promise(function(resolve, reject) {
        var url = BASE_URL + version + '/' + object;
        request({
            url: url,
            timeout: 30000
        }, function(error, response){
            if(!error) {
                if(response.statusCode !== 200) {
                    reportError(url, url + ' - Error not found! (404)');
                } else {
                    if(path.extname(url) === '.gz') {
                        areGzHeadersCorrect(url, response.headers);
                    } else {
                        areNonGzHeadersCorrect(url, response.headers);
                    }
                }
                resolve(true);
            } else {
                reportError(url, error.code);
                resolve(error);
            }

        });
    });
    return newPromise;
}

function areGzHeadersCorrect(gzUrl, headers) {
    var expectedMimeType = mime.lookup(gzUrl.replace('.gz', ''));
    if(headers['content-type'] !== expectedMimeType) {
        reportError(gzUrl, chalk.red('MIME type incorrect: ') + 'expected ' + chalk.green(expectedMimeType) + ' but found ' + chalk.blue(headers['content-type']));
    }

    if(headers['content-encoding'] !== 'gzip') {
        reportError(gzUrl, gzUrl + chalk.red(' ContentEncoding not set to gzip'));
    }
}

function areNonGzHeadersCorrect(url, headers) {
    var expectedMimeType = mime.lookup(url);
    if(headers['content-type'] !== expectedMimeType) {
        reportError(url, chalk.red('MIME type incorrect: ') + 'expected ' + chalk.green(expectedMimeType) + ' but found ' + chalk.blue(headers['content-type']));
    }
}

function reportError(url, error) {
    console.error(error);
    errors.push({
        url: url,
        error: error
    });
}

function finalReport(errors) {
    if(errors.length > 0) {
        console.error(chalk.red(errors.length + ' errors were found') + ' see above for details');
    }
}


