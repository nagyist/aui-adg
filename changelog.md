# 5.9.0 [Unreleased]

[Documentation](https://docs.atlassian.com/aui/5.9.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.0)

# 5.8.13
* [Documentation](https://docs.atlassian.com/aui/5.8.13/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.13)

## Added
* Added SVG icon files for sidebar to the atlassian-plugin.xml web-resource

## Fixed
* There were some uncompiled ADG LESS files in the plugin.

# 5.8.12
* [Documentation](https://docs.atlassian.com/aui/5.8.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.12)

## Changed
* Product logo images for the ADG header moved from data-uris in the LESS to external images.

## Fixed
* Fixed bug in JSON example in single-select docs.

# 5.8.11
* [Documentation](https://docs.atlassian.com/aui/5.8.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.11)

## Fixed
* JavaScript error in single select documentation's async example.

# 5.8.10

* [Documentation](https://docs.atlassian.com/aui/5.8.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.10)

# 5.8.9

* [Documentation](https://docs.atlassian.com/aui/5.8.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.9)

# 5.8.8

* [Documentation](https://docs.atlassian.com/aui/5.8.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.8)

## Fixed
* Markup examples for the Sidebar are now correct.

# 5.8.7

* [Documentation](https://docs.atlassian.com/aui/5.8.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.7)

# 5.8.6

* [Documentation](https://docs.atlassian.com/aui/5.8.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.6)

## Upgrade Notes
* This release is broken due to cross-branch contamination. Please use 5.8.7 instead.

# 5.8.5

* [Documentation](https://docs.atlassian.com/aui/5.8.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.5)

## Upgrade Notes
* This release is broken due to cross-branch contamination. Please use 5.8.7 instead.

# 5.8.4
# 5.8.4

* [Documentation](https://docs.atlassian.com/aui/5.8.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.4)

# 5.8.3

* [Documentation](https://docs.atlassian.com/aui/5.8.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.3)

# 5.8.2

* [Documentation](https://docs.atlassian.com/aui/5.8.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.2)

## Upgrade Notes
* This release has some missing artifacts, please use 5.8.3 instead.

# 5.8.1

* [Documentation](https://docs.atlassian.com/aui/5.8.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.1)

# 5.8.0

[Documentation](https://docs.atlassian.com/aui/5.8.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.0)

## Upgrade Notes
* aui-ie9.css has been removed from the flatpack. It is no longer necessary to include this file.
* If you are using the sidebar from the flatpack, you will now need to include aui-experimental.js
* The contents of an AUI Dropdown2 can now be created entirely using Soy (instead of sending html to aui.dropdown2.contents)
* The markup generated using the AUI Dropdown2 Soy templates has changed. This new markup pattern is now much more accessible to screen readers.
* Removed dependencies from AUI components to AUI soy templates (including responsive header).
* AUI Sandbox has been removed, in the future we will be uploading examples to jsbin.
* AUI Flag `persistent` option has been removed (deprecated in 5.7.7).  Update all usages of this option to use `close` instead (see http://docs.atlassian.com/aui/5.8.0/flag.html).

## Highlights
* CSS deprecations have been added to the deprecation web-resource (`com.atlassian.auiplugin:aui-deprecation`)
* The docs now have the ability to push code examples to jsbin, codepen and jsfiddle

# 5.7.23
[Documentation](https://docs.atlassian.com/aui/5.7.23/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.23)

# 5.7.22
[Documentation](https://docs.atlassian.com/aui/5.7.22/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.22)

# 5.7.21
[Documentation](https://docs.atlassian.com/aui/5.7.21/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.21)

# 5.7.20
[Documentation](https://docs.atlassian.com/aui/5.7.20/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.20)

# 5.7.19
[Documentation](https://docs.atlassian.com/aui/5.7.19/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.19)

# 5.7.18
[Documentation](https://docs.atlassian.com/aui/5.7.18/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.18)
## Highlights
* Fix iconfont from previous version.

# 5.7.17
[Documentation](https://docs.atlassian.com/aui/5.7.17/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.17)

## Upgrade notes
* This version contains some broken icons and should not be used.

# 5.7.16
[Documentation](https://docs.atlassian.com/aui/5.7.16/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.16)

# 5.7.15
[Documentation](https://docs.atlassian.com/aui/5.7.15/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.15)

# 5.7.14
[Documentation](https://docs.atlassian.com/aui/5.7.14/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.14)
## Upgrade notes
* This version of the AUI plugin uses version 2.2.0 the `com.atlassian.lesscss` lesscss-maven-plugin, with less version 1.7.0. See AUI-3097 for details.

# 5.7.13
[Documentation](https://docs.atlassian.com/aui/5.7.13/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.13)

# 5.7.12
[Documentation](https://docs.atlassian.com/aui/5.7.12/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.12)

# 5.7.11
[Documentation](https://docs.atlassian.com/aui/5.7.11/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.11)

# 5.7.10
[Documentation](https://docs.atlassian.com/aui/5.7.10/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.10)

# 5.7.9
[Documentation](https://docs.atlassian.com/aui/5.7.9/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.9)

# 5.7.8
[Documentation](https://docs.atlassian.com/aui/5.7.8/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.8)

# 5.7.7
[Documentation](https://docs.atlassian.com/aui/5.7.7/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.7)

# 5.7.6
This is a broken release containing no bugfixes and should not be used. Please use the latest 5.7.x release instead

# 5.7.5
[Documentation](https://docs.atlassian.com/aui/5.7.5/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.5)

## Upgrade notes
* A dependency on the platform POM was removed for maven 2. See [DP-289](https://ecosystem.atlassian.net/browse/DP-289)

# 5.7.4
[Documentation](https://docs.atlassian.com/aui/5.7.4/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.4)

# 5.7.3
[Documentation](https://docs.atlassian.com/aui/5.7.3/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.3)

# 5.7.2
[Documentation](https://docs.atlassian.com/aui/5.7.2/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.2)

# 5.7.1
[Documentation](https://docs.atlassian.com/aui/5.7.1/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.1)

# 5.7.0
[Documentation](https://docs.atlassian.com/aui/5.7.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.0)


For all release notes older than 5.7.0, check [EAC](https://extranet.atlassian.com/display/AUI/AUI+Release+Notes)
