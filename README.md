AUI ADG
=======

Extension of AUI to implement the Atlassian Design Guidelines.

Requirements
------------

- Node 0.12+
- NPM 1.3+
- Bower 1.2+
- Java 7
- Maven 3+ ([mvnvm](http://mvnvm.org/) can be used to manage the maven version)

Installation
------------

NPM install takes care of everything for you.

    npm install

To avoid issues between different versions of build tools such as bower or gulp, we use local npm
versions of all build tools instead of globally installed versions. For example, we use

    ./node_modules/.bin/gulp build

instead of

    gulp build

Linking to the AUI source
-------------------------

1. Run `bower link` in your local AUI repo
2. Run `bower link aui` in your local AUI-ADG repo

Note that you must run the `gulp build` task in AUI before running any of the below commands.

Running standalone test pages aka 'flatapp'
-------------------------------------------

To run test pages:

    ./node_modules/.bin/gulp flatapp

Running plugins2 test pages
---------------------------

To run AUI as an Atlassian plugin in the Atlassian Refapp, first build the maven distribution

    ./node_modules/.bin/gulp refapp:install

Then to start the refapp server

    ./node_modules/.bin/gulp refapp

Both of the above commands filter maven output by default. Pass the `--verbose` flag to view the full maven output.

If you wish to install the plugin without running the refapp,
    ./node_modules/.bin/gulp plugin

Running plugins2 test pages
---------------------------

To run integration tests in the Atlassian refapp, run

    ./node_modules/.bin/gulp refapp:integration-test

Note that you need to have run `gulp refapp:install` before running integration tests

6. To install the maven artifact in your product, it is located in `integration/plugin/target/`
7. Make changes and reload pages to see them



Contributing
------------

Create a new branch using this format: `AUI-1234-description-of-the-change`

  - Stable branches [x.x.x] for bug fixes corresponding to the version which the bug affects
  - Feature branches [epic/*] for adding features to an epic

Tests and Documentation live in the AUI repo, update those if modifying AUI source as well

Commit your changes ensuring you add the issue key to the message, eg: git commit -m "AUI-1234 updated README"

Submit a pull request to master or the relevant stable/epic branch


Documenting
---------

To build the docs:

    npm run docs/build

To build and run the docs on a local server:

    npm run docs

Releasing
---------

    ./build/bin/release.sh [release-version] [tag-name] [next-version]


Changelog
---------

* [Changelog](https://bitbucket.org/atlassian/aui-adg/src/master/changelog.md?at=master)
