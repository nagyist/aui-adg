'use strict';

var gulpAutoTask = require('gulp-auto-task');

gulpAutoTask('{*,**/*}.js', {
    base: './bower_components/aui/build/gulp'
});

gulpAutoTask('{*,**/*}.js', {
    base: './build/gulp'
});
