package it.com.atlassian.aui.javascript.pages;

/**
 * @since 4.0
 */
public class MessagesTestPage extends TestPage
{
    public String getUrl()
    {
        return "/plugins/servlet/ajstest/test-pages/pages/messages/";
    }
}
