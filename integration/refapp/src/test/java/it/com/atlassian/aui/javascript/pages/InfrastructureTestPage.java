package it.com.atlassian.aui.javascript.pages;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * TODO: Document this class / interface here
 *
 * @since v4.4
 */
public class InfrastructureTestPage extends TestPage
{
    @ElementBy(id = "jquery-loaded")
    PageElement jQueryLoaded;

    @ElementBy(id = "raphael-loaded")
    PageElement raphaelLoaded;

    @ElementBy(id = "os-loaded")
    PageElement osLoaded;

    @ElementBy(id = "underscore-loaded")
    PageElement underscoreLoaded;

    @ElementBy(id = "eve-loaded")
    PageElement eveLoaded;

    public String getUrl()
    {
        return "/plugins/servlet/ajstest/test-pages/pages/infrastructure/";
    }

    public boolean jQueryIsLoaded()
    {
        return "true".equals(jQueryLoaded.getText());
    }

    public boolean raphaelIsLoaded()
    {
        return "true".equals(raphaelLoaded.getText());
    }

    public boolean osIsLoaded()
    {
        return "true".equals(osLoaded.getText());
    }

    public boolean underscoreIsLoaded()
    {
        return "true".equals(underscoreLoaded.getText());
    }

    public boolean eveIsLoaded()
    {
        return "true".equals(eveLoaded.getText());
    }
}
