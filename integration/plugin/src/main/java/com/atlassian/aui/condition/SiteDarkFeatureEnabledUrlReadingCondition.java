package com.atlassian.aui.condition;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.condition.SimpleUrlReadingCondition;
import com.atlassian.plugin.webresource.condition.UrlReadingCondition;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.sal.api.features.DarkFeatureEnabledCondition;
import com.atlassian.sal.api.features.DarkFeatureManager;

import java.util.Collections;
import java.util.Map;

import static com.atlassian.sal.api.features.ValidFeatureKeyPredicate.checkFeatureKey;

/**
 * A parameterised plugin module condition for enabling modules in the presence of a site dark feature.
 * Pass a param with parameter name "featureKey" containing the dark feature key. Example:
 * <pre>
 *  &lt;web-item key="some-key" section="some/section" weight="1"&gt;
 *      &lt;label key="menu.title"/&gt;
 *      &lt;link&gt;/some/path&lt;/link&gt;
 *      &lt;condition class="com.atlassian.aui.condition.SiteDarkFeatureEnabledUrlReadingCondition"&gt;
 *          &lt;param name="featureKey"&gt;feature.key&lt;/param&gt;
 *      &lt;/condition&gt;
 *  &lt;/web-item&gt;
 * </pre>
 * The feature key is validated using the {@link com.atlassian.sal.api.features.ValidFeatureKeyPredicate}.
 * @see com.atlassian.sal.api.features.ValidFeatureKeyPredicate
 */
public class SiteDarkFeatureEnabledUrlReadingCondition extends SimpleUrlReadingCondition
{
    private static final String FEATURE_KEY_INIT_PARAMETER_NAME = "featureKey";
    private final DarkFeatureManager darkFeatureManager;
    private String featureKey;

    public SiteDarkFeatureEnabledUrlReadingCondition(final DarkFeatureManager darkFeatureManager)
    {
        this.darkFeatureManager = darkFeatureManager;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException
    {
        if (params.containsKey(FEATURE_KEY_INIT_PARAMETER_NAME))
        {
            featureKey = checkFeatureKey(params.get(FEATURE_KEY_INIT_PARAMETER_NAME));
        }
        else
        {
            throw new PluginParseException("Parameter '" + FEATURE_KEY_INIT_PARAMETER_NAME + "' is mandatory.");
        }
    }

    @Override
    protected boolean isConditionTrue()
    {
        try
        {
            return darkFeatureManager.isFeatureEnabledForAllUsers(featureKey);
        }
        catch (RuntimeException e)
        {
            return false;
        }
    }

    @Override
    protected String queryKey()
    {
        return featureKey;
    }
}
