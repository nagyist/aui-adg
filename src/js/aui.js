// Fancy File Input is required by the refapp and assumed to be bundled in AUI.
// It's exposed as a web resource in the plugin.
// We might want to consider not bundling it.
import '../../bower_components/fancy-file-input/dist/fancy-file-input';
import aui from '../../bower_components/aui/src/js/aui';
export default aui;
